1.Always De- init and then initialize any peripherals if it's settings change more than once in code
2.Always Specify peripheral parameters before initialising it with those values.
3.Once the code is generated using CubeMX settings, then the changes made in source files do not affect cube mx
4.Instructor Code is for different controller settings, implement code carefully
5.Always use Timer Input capture in DMA mode as you'll face difficulty in other modes
