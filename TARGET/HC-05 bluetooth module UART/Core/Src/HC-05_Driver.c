/*
 * HC-05_Driver.c
 *
 *  Created on: Jan 14, 2022
 *      Author: GEETANSH
 */
//**********************************************************HC-05 BT MODULE DRIVER SRC FILE*****************************************************
#ifndef HC_05_DRIVER_C
#define HC_05_DRIVER_C

/*
 ***********************************************************INCLUDES SECTION********************************************************************
 */
#include"stm32f4xx_hal.h"
#include"HC-05.h"
#include"string.h"
/*
 ********************************************************PV VARIABLES DEFINE SECTION************************************************************
 */
uint8_t			Btconnected = 0;		//Flag to specify whether BT is connected if value = 1, otherwise not connected if value = 0 in BTInit() function
char  			cmdRxBuf[20];			//Command acknowledgment receive Buffer
uint8_t 		errFlag = 0;			//Flag to determine error in operation if value!=0 in resetBT() Function
UART_HandleTypeDef huart4;
/*
 *********************************************************FUNCTION DEFINITION API'S*************************************************************
 */
/***********************************************************************************************************************************************/
//    Function to Reset the BT module to make it Pairable
void resetBT(void)
{
	errFlag = 0;  //Ensuring error flag is 0

	HAL_GPIO_WritePin(BT_ENABLE_GPIO_Port, BT_ENABLE_Pin, GPIO_PIN_SET);		//Ensuring if Enable Pin is high for entering Command Mode

	HAL_UART_Transmit(&huart4, (uint8_t*)"AT\r\n", sizeof("AT\r\n"), HAL_MAX_DELAY);		//Transmitting Basic Command AT to check connection

	HAL_UART_Receive(&huart4,(uint8_t*)&cmdRxBuf, sizeof(cmdRxBuf), 5000);			//Receiving OK

	if(!(strcmp(cmdRxBuf , "OK\r\n")))
	{
		errFlag += errFlag;
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
	}

	if(errFlag != 1)
	{
		HAL_UART_Transmit(&huart4,(uint8_t*)"AT+RESET\r\n", sizeof("AT+RESET\r\n"), HAL_MAX_DELAY);     //Reset Command to Reset BT Parameters

		HAL_UART_Receive(&huart4,(uint8_t*)&cmdRxBuf, sizeof(cmdRxBuf), 5000);

		if(!(strcmp(cmdRxBuf , "OK\r\n")))
			{
				errFlag += errFlag;
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
			}
	}

	if(errFlag != 2)
	{
		HAL_UART_Transmit(&huart4,(uint8_t*)"AT+CMODE=1\r\n", sizeof("AT+CMODE=1\r\n"), HAL_MAX_DELAY);   //Setting CMODE=1 to Connect to ANY device

		HAL_UART_Receive(&huart4,(uint8_t*)&cmdRxBuf, sizeof(cmdRxBuf), 5000);

		if(!(strcmp(cmdRxBuf , "OK\r\n")))
			{
				errFlag += errFlag;
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
			}
    }

	if(errFlag != 3)
	{
		HAL_UART_Transmit(&huart4,(uint8_t*)"AT+INIT\r\n", sizeof("AT+INIT\r\n"), HAL_MAX_DELAY);         //Initialize the BT

		HAL_UART_Receive(&huart4,(uint8_t*)&cmdRxBuf, sizeof(cmdRxBuf), 5000);

		if(!(strcmp(cmdRxBuf , "OK\r\n")))
			{
				errFlag += errFlag;
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
			}
    }

	if(errFlag != 4)
	{
		HAL_UART_Transmit(&huart4,(uint8_t*)"AT+RMAAD\r\n", sizeof("AT+RMAAD\r\n"), HAL_MAX_DELAY);         //Remove all Previously Paired Devices

		HAL_UART_Receive(&huart4,(uint8_t*)&cmdRxBuf, sizeof(cmdRxBuf), 5000);

		if(!(strcmp(cmdRxBuf , "OK\r\n")))
			{
				errFlag += errFlag;
				HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);
			}
    }

	HAL_Delay(500);

	HAL_UART_Transmit(&huart4,(uint8_t*)"AT+UART=38400,1,0\r\n", sizeof("AT+UART=38400,1,0\r\n"), HAL_MAX_DELAY);         //Baud rate change to 38400

	HAL_Delay(500);


	if(errFlag !=0)
	{
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);				//Error LED gets toggled to indicate fault
	}
}
/*****************************************************************************************************************************************************/
//Function to initialize the BT and Pairing with Wireless Master Device
void BTInit(void)
{
		HAL_UART_DeInit(&huart4); 			//De-initializing UART peripheral

	    HAL_UART_Init(&huart4);    			//Re-initializing UART peripheral according to our config

	    huart4.Instance = UART4;
	    huart4.Init.BaudRate = 38400;       //Baud Rate is set to 38400 in command mode
	    huart4.Init.WordLength = UART_WORDLENGTH_8B;
	    huart4.Init.StopBits = UART_STOPBITS_1;
	    huart4.Init.Parity = UART_PARITY_NONE;
	    huart4.Init.Mode = UART_MODE_TX_RX;
	    huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	    huart4.Init.OverSampling = UART_OVERSAMPLING_16;

	    	//Error Handler
	    if (HAL_UART_Init(&huart4) != HAL_OK)

	    	{
	    		Error_Handler();
	    	}

	if(HAL_GPIO_ReadPin(BT_STATE_GPIO_Port, BT_STATE_Pin) == GPIO_PIN_SET) //Checking if BT is already connected
	{
		Btconnected = 1;
	}

    if(!Btconnected)
    {
    	HAL_GPIO_WritePin(BT_ENABLE_GPIO_Port,BT_ENABLE_Pin, GPIO_PIN_SET);		//Setting the Enable pin HIGH to enter command mode
    }

	//Resetting the controller and sending the Required AT commands
	resetBT();
    //Waiting For Device to pair in a while loop
	while(!Btconnected)
	{

		if(HAL_GPIO_ReadPin(BT_STATE_GPIO_Port, BT_STATE_Pin)== GPIO_PIN_SET) 	//Check If state pin is high to indicate successful Connection
			{
				Btconnected = 1;
			}

	}
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);		//Toggling green LED to indicate successful connection

	HAL_GPIO_WritePin(BT_ENABLE_GPIO_Port, BT_ENABLE_Pin, GPIO_PIN_RESET);	//Entering Data Mode

	HAL_UART_Abort(&huart4);			//Aborting any ongoing UART Transaction

	HAL_UART_DeInit(&huart4); 			//De-initializing UART to Change baud rate for Data Mode

}
/****************************************************************************************************************************************************/
//Function to receive data
void BT_DataMode(void)
{
	HAL_GPIO_WritePin(BT_ENABLE_GPIO_Port, BT_ENABLE_Pin, GPIO_PIN_RESET);	//Entering Data Mode

	HAL_UART_DeInit(&huart4); 			//De-initializing UART to Change baud rate for Data Mode

	HAL_UART_Init(&huart4);    			//Re-initializing UART peripheral according to our config

	huart4.Instance = UART4;
	huart4.Init.BaudRate = 38400;												//Baud rate is set to 9600 in data mode
	huart4.Init.WordLength = UART_WORDLENGTH_8B;
	huart4.Init.StopBits = UART_STOPBITS_1;
	huart4.Init.Parity = UART_PARITY_NONE;
	huart4.Init.Mode = UART_MODE_TX_RX;
	huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart4.Init.OverSampling = UART_OVERSAMPLING_16;

	//Error Handler

	if (HAL_UART_Init(&huart4) != HAL_OK)

	 {
	    Error_Handler();
	 }

}

/*
 ******************************************************************IRQ Handler For UART RECEIVE************************************************************
 */

/*void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
}
*/

#endif


