/*
*
*  HC-05 Bluetooth Module UART driver Header File*
*
*  Author - Geetansh
*
*  Date Created : 14-01-2022
************************************************NOTE:ALWAYS SET USART RX PIN OF CONTROLLER TO PULL UP THROUGH CUBEMX***************************************************
*/
#ifndef HC_05_DRIVER_H
#define HC_05_DRIVER_H

#include "stm32f4xx_hal.h" /*Needed for UART*/
#include "main.h"

/*
 *****************************************************************    DEFINES SECTION    ******************************************************************************
 */

/*
 ******************************************************************   FUNCTION DECLARATIONS    ************************************************************************
 */

void BTInit(void);										//INITIALIZES THE BT MODULE FOR SENDING/RECIEVING DATA
uint8_t BT_sendCommand(const char* cmd);				//SENDS AT COMMANDS OVER COMMAND MODE
void BT_DataMode(void);						//RECIEVES THE DATA OVER DATA MODE
void resetBT(void);										//RESETS THE BLUETOOTH TO MAKE IT PAIRABLE


#endif
