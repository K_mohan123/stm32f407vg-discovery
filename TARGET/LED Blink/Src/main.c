/**
 ******************************************************************************
 * @file           : main.c
 * @author         : Auto-generated by STM32CubeIDE
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <stdint.h>
#include <stdio.h>

uint32_t *portEnable= (uint32_t*)0x40023830;
uint32_t *portMode= (uint32_t*)0x40020C00;
uint32_t *portOutputConfig= (uint32_t*)0x40020C14;


int main(void)
{
//	*portEnable|=0x08; //mask value can be defined by putting 1 only at the required bit and making every other bit as 0, mask value will be 0x00000008
//    *portMode&=0xC0FFFFFF;
//	*portMode|=0x31000000;
//	*portOutputConfig|=0x7000;
      *portEnable|=(1<<3);
      *portMode&=~(0x55<<24);
      *portMode|=(0x55<<24);
//      *portOutputConfig|=(15<<12);


    /* Loop forever */

	for (uint8_t i=0;i<10000;i++){
		*portOutputConfig^=(15<<12);
		for (uint32_t l=0;l<1000000;l++);
	}

	while(1);
}
